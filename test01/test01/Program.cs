﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test01
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "";
            var age = "";

            Console.WriteLine("Your name: ");
            name = Console.ReadLine();

            Console.WriteLine("Your age:");
            age = Console.ReadLine();

            Console.WriteLine($"Your name is {name} and you are {age}");
            Console.WriteLine("{0}, {1}", name, age);
            Console.WriteLine("" + name + ", " + age + " ");

        }
    }
}
